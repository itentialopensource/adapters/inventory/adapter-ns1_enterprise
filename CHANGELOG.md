
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:54PM

See merge request itentialopensource/adapters/adapter-ns1_enterprise!14

---

## 0.4.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-ns1_enterprise!12

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:12PM

See merge request itentialopensource/adapters/adapter-ns1_enterprise!11

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:27PM

See merge request itentialopensource/adapters/adapter-ns1_enterprise!10

---

## 0.4.0 [05-10-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/inventory/adapter-ns1_enterprise!9

---

## 0.3.4 [03-26-2024]

* Changes made at 2024.03.26_14:46PM

See merge request itentialopensource/adapters/inventory/adapter-ns1_enterprise!8

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_14:03PM

See merge request itentialopensource/adapters/inventory/adapter-ns1_enterprise!7

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:13PM

See merge request itentialopensource/adapters/inventory/adapter-ns1_enterprise!6

---

## 0.3.1 [02-26-2024]

* Changes made at 2024.02.26_13:41PM

See merge request itentialopensource/adapters/inventory/adapter-ns1_enterprise!5

---

## 0.3.0 [12-30-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-ns1_enterprise!4

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-ns1_enterprise!3

---

## 0.1.2 [03-11-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/inventory/adapter-ns1_enterprise!2

---

## 0.1.1 [07-08-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/inventory/adapter-ns1_enterprise!1

---
