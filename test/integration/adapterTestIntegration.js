/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-ns1_enterprise',
      type: 'Ns1Enterprise',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Ns1Enterprise = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Ns1_enterprise Adapter Test', () => {
  describe('Ns1Enterprise Class Tests', () => {
    const a = new Ns1Enterprise(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-ns1_enterprise-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-ns1_enterprise-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const operationsPostOpsBootstrapBodyParam = {
      name: 'string'
    };
    describe('#postOpsBootstrap - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOpsBootstrap(operationsPostOpsBootstrapBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.password);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'postOpsBootstrap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let operationsOperatorId = 'fakedata';
    const operationsPostOpsOperatorsOperatorIdBodyParam = {
      name: 'string'
    };
    describe('#postOpsOperatorsOperatorId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOpsOperatorsOperatorId(operationsOperatorId, operationsPostOpsOperatorsOperatorIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.two_factor_auth);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.last_access);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.email);
              } else {
                runCommonAsserts(data, error);
              }
              operationsOperatorId = data.response.id;
              saveMockData('Operations', 'postOpsOperatorsOperatorId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let operationsOrgid = 555;
    const operationsPostOpsOrgsOrgidBodyParam = {
      name: 'My Company'
    };
    describe('#postOpsOrgsOrgid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOpsOrgsOrgid(operationsOrgid, operationsPostOpsOrgsOrgidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2000, data.response.org_id);
                assert.equal('2019-08-05T19:37:08.065Z', data.response.ts_create);
                assert.equal('2019-08-05T19:37:08.065Z', data.response.ts_update);
                assert.equal('My Company', data.response.name);
                assert.equal(2000, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              operationsOrgid = data.response.org_id;
              saveMockData('Operations', 'postOpsOrgsOrgid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const operationsPutOpsOperatorsBodyParam = {
      name: 'string'
    };
    describe('#putOpsOperators - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putOpsOperators(operationsPutOpsOperatorsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'putOpsOperators', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpsOperators - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpsOperators((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'getOpsOperators', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpsOperatorsOperatorId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpsOperatorsOperatorId(operationsOperatorId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.two_factor_auth);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.last_access);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.email);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'getOpsOperatorsOperatorId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const operationsPutOpsOrgsBodyParam = {
      name: 'My Company'
    };
    describe('#putOpsOrgs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putOpsOrgs(operationsPutOpsOrgsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'putOpsOrgs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpsOrgs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpsOrgs((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'getOpsOrgs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpsOrgsOrgid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpsOrgsOrgid(operationsOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2000, data.response.org_id);
                assert.equal('2019-08-05T19:37:08.065Z', data.response.ts_create);
                assert.equal('2019-08-05T19:37:08.065Z', data.response.ts_update);
                assert.equal('My Company', data.response.name);
                assert.equal(2000, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'getOpsOrgsOrgid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpsOrgsOrgidServiceGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpsOrgsOrgidServiceGroups(operationsOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'getOpsOrgsOrgidServiceGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceDefinitionsDefinitionid = 'fakedata';
    const serviceDefinitionsPostOpsServiceDefsDefinitionidBodyParam = {
      nameservers: [
        'string'
      ],
      type: 'string',
      name: 'string'
    };
    describe('#postOpsServiceDefsDefinitionid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOpsServiceDefsDefinitionid(serviceDefinitionsDefinitionid, serviceDefinitionsPostOpsServiceDefsDefinitionidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.config_tree_id);
                assert.equal('object', typeof data.response.properties);
                assert.equal(true, Array.isArray(data.response.nameservers));
                assert.equal('string', data.response.type);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDefinitions', 'postOpsServiceDefsDefinitionid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceDefinitionsPutOpsServiceDefsBodyParam = {
      nameservers: [
        'string'
      ],
      type: 'string',
      name: 'string'
    };
    describe('#putOpsServiceDefs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putOpsServiceDefs(serviceDefinitionsPutOpsServiceDefsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDefinitions', 'putOpsServiceDefs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpsServiceDefs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpsServiceDefs((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDefinitions', 'getOpsServiceDefs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpsServiceDefsDefinitionid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpsServiceDefsDefinitionid(serviceDefinitionsDefinitionid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.config_tree_id);
                assert.equal('object', typeof data.response.properties);
                assert.equal(true, Array.isArray(data.response.nameservers));
                assert.equal('string', data.response.type);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDefinitions', 'getOpsServiceDefsDefinitionid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceGroupsServicegroupid = 555;
    describe('#postOpsServiceGroupsServicegroupid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOpsServiceGroupsServicegroupid(serviceGroupsServicegroupid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.properties);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceGroups', 'postOpsServiceGroupsServicegroupid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceGroupsOrgid = 555;
    describe('#postOpsServiceGroupsServicegroupidOrgOrgid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOpsServiceGroupsServicegroupidOrgOrgid(serviceGroupsServicegroupid, serviceGroupsOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response.status);
                assert.equal('2000', data.response.org_id);
                assert.equal('2', data.response.service_group_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceGroups', 'postOpsServiceGroupsServicegroupidOrgOrgid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceGroupsPutOpsServiceGroupsBodyParam = {
      name: 'string'
    };
    describe('#putOpsServiceGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putOpsServiceGroups(serviceGroupsPutOpsServiceGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceGroups', 'putOpsServiceGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpsServiceGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpsServiceGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceGroups', 'getOpsServiceGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpsServiceGroupsServicegroupid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpsServiceGroupsServicegroupid(serviceGroupsServicegroupid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.properties);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceGroups', 'getOpsServiceGroupsServicegroupid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpsServiceGroupsServicegroupidOrgs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpsServiceGroupsServicegroupidOrgs(serviceGroupsServicegroupid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceGroups', 'getOpsServiceGroupsServicegroupidOrgs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let iPAMAddressId = 555;
    const iPAMPostIpamAddressAddressIdBodyParam = {
      prefix: 'string',
      network_id: 6,
      kvps: {},
      tags: [
        'string'
      ],
      desc: 'string',
      status: 'string',
      name: 'string',
      inherited_tags: [
        'string'
      ],
      total_addresses: 'string',
      children: 9,
      free_addresses: 'string',
      id: 10,
      indent: 4,
      used_addresses: 'string',
      parent: {}
    };
    describe('#postIpamAddressAddressId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamAddressAddressId(iPAMAddressId, iPAMPostIpamAddressAddressIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.prefix);
                assert.equal(8, data.response.network_id);
                assert.equal('object', typeof data.response.kvps);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.desc);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.inherited_tags));
                assert.equal('string', data.response.total_addresses);
                assert.equal(10, data.response.children);
                assert.equal('string', data.response.free_addresses);
                assert.equal(6, data.response.id);
                assert.equal(10, data.response.indent);
                assert.equal('string', data.response.used_addresses);
                assert.equal('object', typeof data.response.parent);
              } else {
                runCommonAsserts(data, error);
              }
              iPAMAddressId = data.response.id;
              saveMockData('IPAM', 'postIpamAddressAddressId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iPAMPostIpamAddressAddressIdMergeBodyParam = {
      root_address_id: 4,
      merged_address_id: 4
    };
    describe('#postIpamAddressAddressIdMerge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamAddressAddressIdMerge(iPAMAddressId, iPAMPostIpamAddressAddressIdMergeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.prefix);
                assert.equal(2, data.response.network_id);
                assert.equal('object', typeof data.response.kvps);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.desc);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.inherited_tags));
                assert.equal('string', data.response.total_addresses);
                assert.equal(7, data.response.children);
                assert.equal('string', data.response.free_addresses);
                assert.equal(9, data.response.id);
                assert.equal(2, data.response.indent);
                assert.equal('string', data.response.used_addresses);
                assert.equal('object', typeof data.response.parent);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'postIpamAddressAddressIdMerge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iPAMPostIpamAddressAddressIdSplitBodyParam = {
      prefix: 'string',
      network_id: 1,
      kvps: {},
      tags: [
        'string'
      ],
      desc: 'string',
      status: 'string',
      name: 'string',
      inherited_tags: [
        'string'
      ],
      total_addresses: 'string',
      children: 6,
      free_addresses: 'string',
      id: 5,
      indent: 10,
      used_addresses: 'string',
      parent: {}
    };
    describe('#postIpamAddressAddressIdSplit - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamAddressAddressIdSplit(iPAMAddressId, iPAMPostIpamAddressAddressIdSplitBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.prefix);
                assert.equal(5, data.response.network_id);
                assert.equal('object', typeof data.response.kvps);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.desc);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.inherited_tags));
                assert.equal('string', data.response.total_addresses);
                assert.equal(3, data.response.children);
                assert.equal('string', data.response.free_addresses);
                assert.equal(6, data.response.id);
                assert.equal(4, data.response.indent);
                assert.equal('string', data.response.used_addresses);
                assert.equal('object', typeof data.response.parent);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'postIpamAddressAddressIdSplit', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let iPAMSubnetId = 555;
    let iPAMPoolId = 555;
    const iPAMPostIpamAddressSubnetIdPoolPoolIdBodyParam = {
      range: 'string',
      id: 9,
      options: [
        {
          name: 'string',
          value: 2,
          always_send: false
        }
      ]
    };
    describe('#postIpamAddressSubnetIdPoolPoolId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamAddressSubnetIdPoolPoolId(iPAMSubnetId, iPAMPoolId, iPAMPostIpamAddressSubnetIdPoolPoolIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.range);
                assert.equal(2, data.response.id);
                assert.equal(true, Array.isArray(data.response.options));
              } else {
                runCommonAsserts(data, error);
              }
              iPAMSubnetId = data.response.id;
              iPAMPoolId = data.response.id;
              saveMockData('IPAM', 'postIpamAddressSubnetIdPoolPoolId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iPAMNetworkId = 555;
    const iPAMPostIpamNetworkNetworkIdBodyParam = {
      name: 'string',
      rt: 'string',
      tags: [
        'string'
      ],
      desc: 'string',
      kvps: {},
      free_addresses_v4: 'string',
      used_addresses_v4: 'string',
      total_addresses_v4: 'string',
      num_prefixes_v4: 'string',
      free_addresses_v6: 'string',
      used_addresses_v6: 'string',
      total_addresses_v6: 'string',
      num_prefixes_v6: 'string',
      addresses: [
        {
          addr_id: 0,
          prefix: 'string',
          name: 'string'
        }
      ]
    };
    describe('#postIpamNetworkNetworkId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamNetworkNetworkId(iPAMNetworkId, iPAMPostIpamNetworkNetworkIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.rt);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.desc);
                assert.equal('object', typeof data.response.kvps);
                assert.equal('string', data.response.free_addresses_v4);
                assert.equal('string', data.response.used_addresses_v4);
                assert.equal('string', data.response.total_addresses_v4);
                assert.equal('string', data.response.num_prefixes_v4);
                assert.equal('string', data.response.free_addresses_v6);
                assert.equal('string', data.response.used_addresses_v6);
                assert.equal('string', data.response.total_addresses_v6);
                assert.equal('string', data.response.num_prefixes_v6);
                assert.equal(true, Array.isArray(data.response.addresses));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'postIpamNetworkNetworkId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iPAMPutIpamAddressBodyParam = {
      prefix: 'string',
      network_id: 3,
      kvps: {},
      tags: [
        'string'
      ],
      desc: 'string',
      status: 'string',
      name: 'string',
      inherited_tags: [
        'string'
      ],
      total_addresses: 'string',
      children: 2,
      free_addresses: 'string',
      id: 5,
      indent: 3,
      used_addresses: 'string',
      parent: {}
    };
    describe('#putIpamAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIpamAddress(iPAMPutIpamAddressBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'putIpamAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddress((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'getIpamAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddressSearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddressSearch(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'getIpamAddressSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddressAddressId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddressAddressId(iPAMAddressId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.prefix);
                assert.equal(10, data.response.network_id);
                assert.equal('object', typeof data.response.kvps);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.desc);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.inherited_tags));
                assert.equal('string', data.response.total_addresses);
                assert.equal(2, data.response.children);
                assert.equal('string', data.response.free_addresses);
                assert.equal(4, data.response.id);
                assert.equal(4, data.response.indent);
                assert.equal('string', data.response.used_addresses);
                assert.equal('object', typeof data.response.parent);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'getIpamAddressAddressId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddressAddressIdAdjacent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddressAddressIdAdjacent(iPAMAddressId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.prefix);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'getIpamAddressAddressIdAdjacent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddressAddressIdChildren - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddressAddressIdChildren(iPAMAddressId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'getIpamAddressAddressIdChildren', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddressAddressIdParent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddressAddressIdParent(iPAMAddressId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.prefix);
                assert.equal(5, data.response.network_id);
                assert.equal('object', typeof data.response.kvps);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.desc);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.inherited_tags));
                assert.equal('string', data.response.total_addresses);
                assert.equal(10, data.response.children);
                assert.equal('string', data.response.free_addresses);
                assert.equal(8, data.response.id);
                assert.equal(6, data.response.indent);
                assert.equal('string', data.response.used_addresses);
                assert.equal('object', typeof data.response.parent);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'getIpamAddressAddressIdParent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iPAMPutIpamAddressSubnetIdPoolBodyParam = {
      range: 'string',
      id: 4,
      options: [
        {
          name: 'string',
          value: 7,
          always_send: false
        }
      ]
    };
    describe('#putIpamAddressSubnetIdPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIpamAddressSubnetIdPool(iPAMSubnetId, iPAMPutIpamAddressSubnetIdPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'putIpamAddressSubnetIdPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddressSubnetIdPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddressSubnetIdPool(iPAMSubnetId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'getIpamAddressSubnetIdPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddressSubnetIdPoolPoolId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddressSubnetIdPoolPoolId(iPAMSubnetId, iPAMPoolId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.range);
                assert.equal(8, data.response.id);
                assert.equal(true, Array.isArray(data.response.options));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'getIpamAddressSubnetIdPoolPoolId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamKeys - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamKeys((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'getIpamKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iPAMPutIpamNetworkBodyParam = {
      name: 'string',
      rt: 'string',
      tags: [
        'string'
      ],
      desc: 'string',
      kvps: {},
      free_addresses_v4: 'string',
      used_addresses_v4: 'string',
      total_addresses_v4: 'string',
      num_prefixes_v4: 'string',
      free_addresses_v6: 'string',
      used_addresses_v6: 'string',
      total_addresses_v6: 'string',
      num_prefixes_v6: 'string',
      addresses: [
        {
          addr_id: 0,
          prefix: 'string',
          name: 'string'
        }
      ]
    };
    describe('#putIpamNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIpamNetwork(iPAMPutIpamNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'putIpamNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamNetwork((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'getIpamNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamNetworkNetworkId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamNetworkNetworkId(iPAMNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.rt);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.desc);
                assert.equal('object', typeof data.response.kvps);
                assert.equal('string', data.response.free_addresses_v4);
                assert.equal('string', data.response.used_addresses_v4);
                assert.equal('string', data.response.total_addresses_v4);
                assert.equal('string', data.response.num_prefixes_v4);
                assert.equal('string', data.response.free_addresses_v6);
                assert.equal('string', data.response.used_addresses_v6);
                assert.equal('string', data.response.total_addresses_v6);
                assert.equal('string', data.response.num_prefixes_v6);
                assert.equal(true, Array.isArray(data.response.addresses));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'getIpamNetworkNetworkId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamNetworkNetworkIdReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamNetworkNetworkIdReport(iPAMNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.v4);
                assert.equal('object', typeof data.response.v6);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'getIpamNetworkNetworkIdReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamTags((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'getIpamTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let dHCPSpace = 'fakedata';
    let dHCPKey = 'fakedata';
    const dHCPPostDhcpOptiondefSpaceKeyBodyParam = {
      code: 7,
      description: 'string',
      space: 'string',
      friendly_name: 'string',
      key: 'string',
      schema: {},
      standard: false
    };
    describe('#postDhcpOptiondefSpaceKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDhcpOptiondefSpaceKey(dHCPSpace, dHCPKey, dHCPPostDhcpOptiondefSpaceKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.code);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.space);
                assert.equal('string', data.response.friendly_name);
                assert.equal('string', data.response.key);
                assert.equal('object', typeof data.response.schema);
                assert.equal(false, data.response.standard);
              } else {
                runCommonAsserts(data, error);
              }
              dHCPSpace = data.response.space;
              dHCPKey = data.response.key;
              saveMockData('DHCP', 'postDhcpOptiondefSpaceKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let dHCPReservationId = 555;
    const dHCPPostDhcpReservationReservationIdBodyParam = {
      name: 'string',
      value: [
        'string'
      ],
      options: [
        {}
      ],
      address_id: 4
    };
    describe('#postDhcpReservationReservationId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDhcpReservationReservationId(dHCPReservationId, dHCPPostDhcpReservationReservationIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(9, data.response.dhcp_service_id);
                assert.equal('object', typeof data.response.dhcpv4);
                assert.equal('object', typeof data.response.dhcpv6);
                assert.equal(9, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              dHCPReservationId = data.response.id;
              saveMockData('DHCP', 'postDhcpReservationReservationId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let dHCPScopeID = 555;
    const dHCPPostDhcpScopeScopeIDBodyParam = {
      address_id: 8,
      options: [
        {}
      ],
      name: 'string',
      value: [
        'string'
      ]
    };
    describe('#postDhcpScopeScopeID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDhcpScopeScopeID(dHCPScopeID, dHCPPostDhcpScopeScopeIDBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.address_id);
                assert.equal(6, data.response.scope_group_id);
                assert.equal(true, Array.isArray(data.response.options));
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal(2, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              dHCPScopeID = data.response.id;
              saveMockData('DHCP', 'postDhcpScopeScopeID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let dHCPScopegroupID = 555;
    const dHCPPostDhcpScopegroupScopegroupIDBodyParam = {
      name: 'string',
      dhcp_service_id: 1,
      dhcpv4: {},
      dhcpv6: {},
      id: 10
    };
    describe('#postDhcpScopegroupScopegroupID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDhcpScopegroupScopegroupID(dHCPScopegroupID, dHCPPostDhcpScopegroupScopegroupIDBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(7, data.response.dhcp_service_id);
                assert.equal('object', typeof data.response.dhcpv4);
                assert.equal('object', typeof data.response.dhcpv6);
                assert.equal(9, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              dHCPScopegroupID = data.response.id;
              saveMockData('DHCP', 'postDhcpScopegroupScopegroupID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpLease - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpLease((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.prefix);
                assert.equal(7, data.response.scope_group_id);
                assert.equal('string', data.response.dhcid);
                assert.equal('string', data.response.mac);
                assert.equal('string', data.response.expiration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getDhcpLease', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpOptiondef - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpOptiondef((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getDhcpOptiondef', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPPutDhcpOptiondefSpaceKeyBodyParam = {
      code: 1,
      description: 'string',
      space: 'string',
      friendly_name: 'string',
      key: 'string',
      schema: {},
      standard: false
    };
    describe('#putDhcpOptiondefSpaceKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDhcpOptiondefSpaceKey(dHCPSpace, dHCPKey, dHCPPutDhcpOptiondefSpaceKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'putDhcpOptiondefSpaceKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPPutDhcpReservationBodyParam = {
      name: 'string',
      value: [
        'string'
      ],
      options: [
        {}
      ],
      address_id: 9
    };
    describe('#putDhcpReservation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDhcpReservation(dHCPPutDhcpReservationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'putDhcpReservation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpReservation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpReservation((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getDhcpReservation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpReservationReservationId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpReservationReservationId(dHCPReservationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal(true, Array.isArray(data.response.options));
                assert.equal('string', data.response.mac);
                assert.equal(6, data.response.scope_group_id);
                assert.equal(2, data.response.address_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getDhcpReservationReservationId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPPutDhcpScopeBodyParam = {
      address_id: 8,
      options: [
        {}
      ],
      name: 'string',
      value: [
        'string'
      ]
    };
    describe('#putDhcpScope - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDhcpScope(dHCPPutDhcpScopeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'putDhcpScope', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpScope - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpScope((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getDhcpScope', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPPutDhcpScopeGroupBodyParam = {
      name: 'string',
      dhcp_service_id: 2,
      dhcpv4: {},
      dhcpv6: {},
      id: 2
    };
    describe('#putDhcpScopeGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDhcpScopeGroup(dHCPPutDhcpScopeGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'putDhcpScopeGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpScopeGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpScopeGroup((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getDhcpScopeGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpScopeScopeID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpScopeScopeID(dHCPScopeID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.address_id);
                assert.equal(2, data.response.scope_group_id);
                assert.equal(true, Array.isArray(data.response.options));
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.value));
                assert.equal(2, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getDhcpScopeScopeID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpScopegroupScopegroupID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpScopegroupScopegroupID(dHCPScopegroupID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.dhcp_service_id);
                assert.equal('object', typeof data.response.dhcpv4);
                assert.equal('object', typeof data.response.dhcpv6);
                assert.equal(5, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getDhcpScopegroupScopegroupID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpScopegroupScopegroupIDReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpScopegroupScopegroupIDReport(dHCPScopegroupID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.v4);
                assert.equal('object', typeof data.response.v6);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getDhcpScopegroupScopegroupIDReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const activeDirectoryPostActivedirectoryBodyParam = {
      server: 'string',
      domain: 'string',
      user: 'string',
      password: 'string',
      secure: false
    };
    describe('#postActivedirectory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postActivedirectory(activeDirectoryPostActivedirectoryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.server);
                assert.equal(2, data.response.port);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.base_dn);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.password);
                assert.equal(true, data.response.secure);
                assert.equal('string', data.response.tls_cert);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveDirectory', 'postActivedirectory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const activeDirectoryPostActivedirectoryTestBodyParam = {
      server: 'string',
      domain: 'string',
      user: 'string',
      password: 'string',
      secure: false
    };
    describe('#postActivedirectoryTest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postActivedirectoryTest(activeDirectoryPostActivedirectoryTestBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_enterprise-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveDirectory', 'postActivedirectoryTest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const activeDirectoryPutActivedirectoryBodyParam = {
      server: 'string',
      domain: 'string',
      user: 'string',
      password: 'string',
      secure: true
    };
    describe('#putActivedirectory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putActivedirectory(activeDirectoryPutActivedirectoryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveDirectory', 'putActivedirectory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActivedirectory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getActivedirectory((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.server);
                assert.equal(6, data.response.port);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.base_dn);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.password);
                assert.equal(false, data.response.secure);
                assert.equal('string', data.response.tls_cert);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveDirectory', 'getActivedirectory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpsOperatorsOperatorId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOpsOperatorsOperatorId(operationsOperatorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_enterprise-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'deleteOpsOperatorsOperatorId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const operationsToken = 'fakedata';
    describe('#deleteOpsOrgsOrgid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOpsOrgsOrgid(operationsOrgid, operationsToken, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_enterprise-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'deleteOpsOrgsOrgid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpsServiceDefsDefinitionid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOpsServiceDefsDefinitionid(serviceDefinitionsDefinitionid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_enterprise-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDefinitions', 'deleteOpsServiceDefsDefinitionid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpsServiceGroupsServicegroupid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOpsServiceGroupsServicegroupid(serviceGroupsServicegroupid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_enterprise-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceGroups', 'deleteOpsServiceGroupsServicegroupid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamAddressAddressId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIpamAddressAddressId(iPAMAddressId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'deleteIpamAddressAddressId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamAddressSubnetIdPoolPoolId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIpamAddressSubnetIdPoolPoolId(iPAMSubnetId, iPAMPoolId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_enterprise-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'deleteIpamAddressSubnetIdPoolPoolId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamNetworkNetworkId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIpamNetworkNetworkId(iPAMNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_enterprise-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPAM', 'deleteIpamNetworkNetworkId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpOptiondefSpaceKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDhcpOptiondefSpaceKey(dHCPSpace, dHCPKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_enterprise-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'deleteDhcpOptiondefSpaceKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpReservationReservationId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDhcpReservationReservationId(dHCPReservationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_enterprise-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'deleteDhcpReservationReservationId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpScopeScopeID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDhcpScopeScopeID(dHCPScopeID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'deleteDhcpScopeScopeID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpScopegroupScopegroupID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDhcpScopegroupScopegroupID(dHCPScopegroupID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'deleteDhcpScopegroupScopegroupID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteActivedirectory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteActivedirectory((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_enterprise-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveDirectory', 'deleteActivedirectory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
