# NS1 Enterprise

Vendor: IBM
Homepage: https://www.ibm.com/us-en

Product: NS1 Enterprise DDI
Product Page: https://www.ibm.com/products/ns1-connect

## Introduction
We classify NS1 Enterprise into the Inventory and Network Services domains as NS1 Enterprise provides information on IP Addresses and DHCP. 

"The NS1 Enterprise DDI Solution is a software based offering that spans internal DNS, DHCP, IP Address
Management, and traffic steering for enterprises looking to modernize their DDI infrastructure." 

## Why Integrate
The NS1 Enterprise adapter from Itential is used to integrate the Itential Automation Platform (IAP) with IBM NS1 Enterprise DDI to offer a DNS, DHCP and IPAM solution for configuration and orchestration. 

With this adapter you have the ability to perform operations with NS1 Enterprise such as:

- IPAM
- DHCP

## Additional Product Documentation
The [API documents for IBM NS1](https://developer.ibm.com/apis/catalog/ns1--ibm-ns1-connect-api/Introduction)
The [Information for IBM NS1](https://ns1.com/writable/resources/ns1-ds-NS1-Products.pdf)
